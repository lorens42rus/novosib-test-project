from django.db.models import Count
from ajax_select import register, LookupChannel

from .models import Uchastnik

@register('uchastniki')
class UchastnikiLookup(LookupChannel):

    model = Uchastnik

    def get_query(self, q, request):
        if len(q) < 3:
            return Uchastnik.objects.none()

        q = q[0].title() + q[1:]

        queryset = self.model.get_with_storonapodelu() \
            .filter(uchastnik__istartswith=q)[:10]

        return queryset

    def check_auth(self, request):
        return True
