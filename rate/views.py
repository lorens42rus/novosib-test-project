from django.views import View
from django.shortcuts import render

from .forms import SearchUchastnikForm
from .models import Uchastnik


def index(request):

    person_name = request.GET.get('person')
    return render(request, 'index.html', locals())


class SearchView(View):
    template_name = 'search.html'

    def post(self, request):

        form = SearchUchastnikForm(data=request.POST)
        return render(
            request,
            self.template_name,
            context={
                'search_uchastnik_form': form,
                'queryset': form.get_queryset(),
            })
