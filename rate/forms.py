from django import forms
from django.db.models import Count
from ajax_select import make_ajax_field
from ajax_select.fields import AutoCompleteField

from .models import Uchastnik


class SearchUchastnikForm(forms.Form):
    search = AutoCompleteField(
        'uchastniki',
        required=True,
        help_text=None,
        plugin_options={'minLength': 3},
        min_length=3)

    def get_queryset(self):
        if not self.is_valid():
            return Uchastnik.objects.none()

        return Uchastnik \
            .get_with_storonapodelu() \
            .filter(uchastnik__istartswith=self.cleaned_data.get('search'))
