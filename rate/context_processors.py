from .forms import SearchUchastnikForm


def search_uchastnik(request):
    return {
        'search_uchastnik_form': SearchUchastnikForm(),
    }
