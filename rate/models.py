from django.db import models


class Uchastnik(models.Model):

    uchastnik = models.CharField(max_length=150, unique=True)

    @staticmethod
    def get_with_storonapodelu():
        return Uchastnik.objects \
            .annotate(
                storonapodelu_count=models.Count('storonapodelu')) \
            .exclude(
                storonapodelu_count=0)

    def __str__(self):
        return self.uchastnik


class StoronaPoDelu(models.Model):

    uchastnik = models.ForeignKey(
        Uchastnik, verbose_name='Участник', on_delete = models.CASCADE)
